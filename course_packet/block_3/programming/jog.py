# Two stage example Virtual Machine file
# moves get set in Main
# usb port needs to be set in initInterfaces
# Nadya Peek Dec 2014

#------IMPORTS-------
from pygestalt import nodes
from pygestalt import interfaces
from pygestalt import machines
from pygestalt import functions
from pygestalt.machines import elements
from pygestalt.machines import kinematics
from pygestalt.machines import state
from pygestalt.utilities import notice
from pygestalt.publish import rpc	#remote procedure call dispatcher
import time
import io


#------VIRTUAL MACHINE------
class virtualMachine(machines.virtualMachine):
	
	def initInterfaces(self):
		if self.providedInterface: self.fabnet = self.providedInterface		#providedInterface is defined in the virtualMachine class.
		else: self.fabnet = interfaces.gestaltInterface('FABNET', interfaces.serialInterface(baudRate = 115200, interfaceType = 'ftdi', portName = '/dev/ttyUSB0'))
		
	def initControllers(self):
		self.xAxisNode = nodes.networkedGestaltNode('X Axis', self.fabnet, filename = '086-005a.py', persistence = self.persistence)
		self.yAxisNode = nodes.networkedGestaltNode('Y Axis', self.fabnet, filename = '086-005a.py', persistence = self.persistence)

		self.xyNode = nodes.compoundNode(self.xAxisNode, self.yAxisNode)

	def initCoordinates(self):
		self.position = state.coordinate(['mm', 'mm'])
	
	def initKinematics(self):
		self.xAxis = elements.elementChain.forward([elements.microstep.forward(4), elements.stepper.forward(1.8), elements.leadscrew.forward(8), elements.invert.forward(True)])
		self.yAxis = elements.elementChain.forward([elements.microstep.forward(4), elements.stepper.forward(1.8), elements.leadscrew.forward(8), elements.invert.forward(True)])		
		self.stageKinematics = kinematics.direct(2)	#direct drive on all axes
	
	def initFunctions(self):
		self.move = functions.move(virtualMachine = self, virtualNode = self.xyNode, axes = [self.xAxis, self.yAxis], kinematics = self.stageKinematics, machinePosition = self.position,planner = 'null')
		self.jog = functions.jog(self.move)	#an incremental wrapper for the move function
		pass
		
	def initLast(self):
		#self.machineControl.setMotorCurrents(aCurrent = 0.8, bCurrent = 0.8, cCurrent = 0.8)
		#self.xNode.setVelocityRequest(0)	#clear velocity on nodes. Eventually this will be put in the motion planner on initialization to match state.
		pass
	
	def publish(self):
		#self.publisher.addNodes(self.machineControl)
		pass
	
	def getPosition(self):
		return {'position':self.position.future()}
	
	def setPosition(self, position  = [None]):
		self.position.future.set(position)

	def setSpindleSpeed(self, speedFraction):
		#self.machineControl.pwmRequest(speedFraction)
		pass

#------IF RUN DIRECTLY FROM TERMINAL------
if __name__ == '__main__':
	# The persistence file remembers the node you set. It'll generate the first time you run the
	# file. If you are hooking up a new node, delete the previous persistence file.
	stages = virtualMachine(persistenceFile = "test.vmp")

	# You can load a new program onto the nodes if you are so inclined. This is currently set to 
	# the path to the 086-005 repository on Nadya's machine. 
	#stages.xyNode.loadProgram('../../../086-005/086-005a.hex')
	
	# This is a widget for setting the potentiometer to set the motor current limit on the nodes.
	# The A4982 has max 2A of current, running the widget will interactively help you set. 
	#stages.xyNode.setMotorCurrent(0.7)

	# This is for how fast the 
	stages.xyNode.setVelocityRequest(8)	
	
	# 201510 
	# is_number() checks if the object is a float.
	def is_number(s):
		try:
			float(s)
			return True
		except ValueError:
			return False	

	print "Type x -Enter- then the distance to move -Enter-"
	print "Type y -Enter- then the distance to move -Enter-"
	print "Type <a|z> to move xAxis fwd/backward"
    	print "Type <s|w> to move yAxis up/down"
	print "Type <q> when done with calibration"

	x = y = 0
	up = float(-45)
	left = float(-68.5)
	down = -up
	right = -left

	while True:
        	ch = raw_input("x[a,z], y[s,w], q :")
        	if ch == 'q':
        	    break
		elif ch == 'a':
		    stages.jog([-2, 0])
		elif ch == 'z':
		    stages.jog([2, 0])
		elif ch == 's':
		    stages.jog([0, -2])
		elif ch == 'w':
		    stages.jog([0, 2])
		elif ch == '4.':
		    stages.jog([-68.5, 0])
		elif ch == '6.':
		    stages.jog([68.5, 0])
		elif ch == '8.':
		    stages.jog([0, -45])
		elif ch == '2.':
		    stages.jog([0, 45])
		elif ch == '1':
		    stages.jog([0, up])
		    stages.jog([left, 0])
		    stages.jog([0, down])
		    time.sleep(12)
		    stages.jog([0, up])
		    stages.jog([right, 0])
		    stages.jog([0, down])
		elif ch == '2':
		    stages.jog([0, up])
		    stages.jog([2*left, 0])
		    stages.jog([0, down])
		    time.sleep(14)
		    stages.jog([0, up])
		    stages.jog([2*right, 0])
		    stages.jog([0, down])
		elif ch == '3':
		    stages.jog([0, up])
		    stages.jog([3*left, 0])
		    stages.jog([0, down])
		    time.sleep(16)
		    stages.jog([0, up])
		    stages.jog([3*right, 0])
		    stages.jog([0, down])
		elif ch == '0':
		    stages.jog([0, down])
		elif ch == '0.':
		    stages.jog([0, up])

		elif ch == 'x': # need to catch an ValueError that cannot concentate to a float.
			x1 = float(raw_input());
			if is_number(x1) == True:
				stages.jog([x1,0])
				print "Machine jogged "+str(x1) + " [mm] in the x direction."
			else:
				pass
		elif ch == 'y':
			y1 = float(raw_input());
			if is_number(y1) == True:
				stages.jog([0,y1])
		    		print "Machine jogged "+str(y1) + " [mm] in the y direction."
			else:
				pass
		else:
		    pass
	stages.initCoordinates()
	stages.initFunctions()

	# input from command line
	#print "jog x [mm]"	
	#x = float(raw_input());
	#print "jog y [mm]"
	#y = float(raw_input());
		
	#moves = [[x,y]]
	
	# Move!
	
#	for move in moves:
#		stages.move(move, 0)
#		status = stages.xAxisNode.spinStatusRequest()
		# This checks to see if the move is done.
#		while status['stepsRemaining'] > 0:
#			time.sleep(0.001)
#			status = stages.xAxisNode.spinStatusRequest()	
	
	
