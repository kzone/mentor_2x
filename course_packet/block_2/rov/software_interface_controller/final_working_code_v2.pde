/* 

Fab Lab Robotics Kit
This code is uses the following resources:
https://www.arduino.cc/en/Tutorial/Graph
http://web.ics.purdue.edu/~fwinkler/AD32600_F14/Processing_motor_control.pdf
http://www.instructables.com/id/Double-Sensor-Graphs-With-Processing/?ALLSTEPS
http://www.sojamo.com/libraries/controlP5/reference/controlP5/Slider.html
https://itp.nyu.edu/physcomp/labs/labs-serial-communication/serial-output-from-an-arduino/
http://faculty.purchase.edu/joseph.mckay/physicalcomputing2015/serial.php

*note- the initial version of this code attempted to use firmata; this approach has since proven ineffective for this kit

Before running this code, you need to upload the "final_working_code" example in Arduino
Check:
- BUAD rate in Arduino sketch matches BAUD rate in processing sketch

Troubleshooting: right now only works with graph sketch...

IMPORTANT NEWS!!!!!!!!!!!!!
I THINK I NOW CAN SET UP:  SERIAL 1, SERIAL 2, ..ETC INSTEAD OF USING ARDUINO/MYPORT BECAUSE I AM NOT USING FIRMATA AND HARDCODING THE OUTPUTS IN ARDUINO

*/



//KZ: importing the required libraries----------------------------------------------------------------
import processing.serial.*;
// import cc.arduino.*;
import controlP5.*;
ControlP5 controlP5;    //KZ: create an object from the "ControlP5" class
// Arduino arduino;        //KZ: create an object from the "Arduino" class; "arduino" is the name of the (arduino or serial???) object which sends data to a serial port?????  (NOTE: needs to have its own dedicated port)
Serial myPort_bluetooth;          //KZ: create an object from the "Serial" class; "myPort" is the name of the serial object which listens to a serial port for incoming data (NOTE: needs to have its own dedicated port)
Serial myPort_usb;


//KZ: declaring golbal variables----------------------------------------------------------------------
//KZ: declaring the varialbes for output (motors)
//int DC_speed = 10; // 0-255       //KZ: "int" means integer
//int DC2_speed = 10; // 0-255
//int DC3_speed = 10; // 0-255
// KZ: declaring the variables for input (sensor)
int xPos = 400;         // KZ: horizontal starting position of the graph, positioned at midway
float inByte = 0;

// int DC_speed = 10; // 0-255       //KZ: "int" means integer
// int DC2_speed = 10; // 0-255
// int DC3_speed = 10; // 0-255

PShape rov;   // KZ: add graphic using PShape
 
boolean hilow = true;    //KZ: 1 of 3 from here: http://faculty.purchase.edu/joseph.mckay/physicalcomputing2015/serial.php


//KZ: setup ------------------------------------------------------------------------------------------
void setup () {
  
  // KZ: size of processing GUI window
  size(800, 400);

//KZ: add graphic; note that in order for the "svg" file to load, it must be in the same folder as this piece of code
 rov = loadShape("rov.svg");   

  // KZ: These lines list the availabe serial ports (need two ports, but not sure if I need this twice)
  println(Serial.list());


//KZ: seting up our serial objects to listen to thier respective ports
//KZ: make sure baud rate matches the one set in Arduino
 myPort_bluetooth = new Serial(this, Serial.list()[0], 57600);
 myPort_usb = new Serial(this, Serial.list()[7], 57600);
  
  // don't generate a serialEvent() unless you get a newline character:
  myPort_bluetooth.bufferUntil('\n');

  // set inital background:
 // background(0);
 
  // for (int i = 0; i <= 13; i++)   //KZ: "i++" indicates that the variable "i" increases by one each time the loop runs
 //  arduino.pinMode(i, Arduino.OUTPUT);     //KZ: need to set the pinmode for the arduino object; not sure if I now need to set this in Arduino since I am not using firmata
 
 
 //controlP5 = new ControlP5(this);
 //controlP5.addSlider;            // ("DC_speed",0,255,DC_speed,151,200,8,80);
 //controlP5.addSlider;           //  ("DC2_speed",0,255,DC2_speed,260,200,8,80);
 //controlP5.addSlider;            //("DC3_speed",0,255,DC3_speed,205,100,8,80);

// KZ: creating a rectangle to map the sensor graph to
 fill(100);                    
 rect(400,0,400,400);
 
 shape(rov, 0, 0, 375, 375);    // KZ: add graphic; the first two values represent an offset from the origin (upper left corner); do not include an offset if you want the image to be centered

 
}
void draw () {
 
  
 //KZ: 2 of 3 from here: http://faculty.purchase.edu/joseph.mckay/physicalcomputing2015/serial.php
      while (myPort_usb.available () > 0) {
      int inByte = myPort_usb.read();
      fill(inByte);
    }
   // rect (0, 0, width, height); 
    if (hilow == true) {
      myPort_usb.write('H');
    }
    else {
      myPort_usb.write('L');
    }
  
  
  
  
  
  
// arduino.analogWrite(3, DC_speed);        // on the custom motor-driver board, pins 3&9 are a pair
// arduino.analogWrite(5, DC2_speed);        // on the custom motor-driver board, pins 5&6 are a pair
// arduino.analogWrite(10, DC3_speed);      // on the custom motor-driver board, pins 10&11 are a pair

 
 /* 
 // KZ: all of the items below are related to the sensor/graph
 
  // draw the line:
  stroke(127, 34, 255);
  line(xPos, height, xPos, height - inByte);    //KZ: need to map this to the rectangle

  // KZ: at the end of the screen, go back halfway to restart graph
  if (xPos >= width) {
    xPos = 400;
   fill(100);                  //KZ: now that we are using a rectangle to map the graph to, we can use "fill" instead of "background"
    rect(400,0,400,400);       //KZ: don't think I need this bc I already set up a rectanle in the setup loop
  } else {
    // increment the horizontal position:
    xPos++;
  }
  */
  
}

/*
void serialEvent (Serial myPort) {
  // get the ASCII string:
  String inString = myPort.readStringUntil('\n');            //KZ: read the incoming data from the serial port

  if (inString != null) {
    // trim off any whitespace:
    inString = trim(inString);
    // convert to an int and map to the screen height:
    inByte = float(inString);
    println(inByte);                                         //KZ: prints the value received from the serial port to the Processing console
    inByte = map(inByte, 0, 1023, 0, height);
  }
}
*/



//KZ: 3 of 3 from here: http://faculty.purchase.edu/joseph.mckay/physicalcomputing2015/serial.php

  void mouseReleased() {
    hilow = !hilow;
  }
  
